"""Initialize the message queue."""
import os

from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from cki_lib.session import get_session
from datawarehouse import Datawarehouse

UMB_MESSENGER_EXCHANGE = os.environ.get('UMB_MESSENGER_EXCHANGE',
                                        'cki.exchange.datawarehouse.kcidb')
UMB_MESSENGER_QUEUE = os.environ.get('UMB_MESSENGER_QUEUE',
                                     'cki.queue.datawarehouse.kcidb.umb_messenger')

QUEUE = MessageQueue()

DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL', 'http://localhost')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN_UMB_MESSENGER')

LOGGER = get_logger('cki.umb_messenger')
SESSION = get_session('cki.umb_messenger')

DATAWAREHOUSE = Datawarehouse(DATAWAREHOUSE_URL, token=DATAWAREHOUSE_TOKEN,
                              session=SESSION)
