# umb-messenger

Webhook responsible for UMB message sending. Messages can be sent upon
completion of the specified stage or finished pipelines.

## Currently supported messages

* **OSCI:** For details about schema and use case, check out [Fedora CI messages].
  Messages are sent upon pipeline completion, if the pipeline was configured to
  support the messages. Both running and complete (or error) messages are supported.

  **BE SURE TO RUN THE MESSAGE VALIDATION BEFORE CHANGING ANYTHING IN THE MESSAGES!**
  The validation script and guidelines are available [on Pagure].

* **Ready for test:** For details about testing CKI builds and submitting
  results to CKI, see the [CKI documentation]

## How to run this project

The messenger can be deployed locally by running

```bash
python -m umb_messenger --queue
```

Note that you need the appropriate environment variables exposed and have UMB
certificate and configuration in the right place!

Required environment variables:

* `SSL_PEM_PATH`: UMB certificate, defaults to `/etc/cki/umb/cki-bot-prod.pem`
* `UMB_CONFIG_PATH`: UMB configuration, defaults to `/etc/cki/umb-config/umb.yml`
* `CKI_LOGGING_LEVEL`: logging level for CKI modules, defaults to WARN; to get
  meaningful output on the command line, set to INFO
* `DATAWAREHOUSE_URL`: URL of the DataWarehouse instance to use, defaults to `localhost`
* `DATAWAREHOUSE_TOKEN_UMB_MESSENGER`: Access token to the DataWarehouse instance

To receive messages via `--queue` from RabbitMQ, the following environment
variables need to be set as well:

* `RABBITMQ_HOST`: space-separated list of AMQP servers
* `RABBITMQ_PORT`: AMQP port
* `RABBITMQ_USER`: user name of the AMQP account
* `RABBITMQ_PASSWORD`: password of the AMQP account
* `UMB_MESSENGER_EXCHANGE`: name of the webhook exchange, defaults to
  `cki.exchange.datawarehouse.kcidb`
* `UMB_MESSENGER_QUEUE`: queue name, defaults to `cki.queue.datawarehouse.kcidb.umb_messenger`

### Manual triggering

Messaging can also be manually triggered via the command line. First, make
sure to have the required environment variables above configured correctly.
Then, the sending of UMB messages can be triggered with

```bash
CKI_LOGGING_LEVEL=INFO \
  CKI_DEPLOYMENT_ENVIRONMENT=production \
  python3 -m umb_messenger \
  --message-type MESSAGE_TYPE \
  --checkout-id CHECKOUT_ID
```

* `MESSAGE_TYPE` can be `osci`, `pre-test` or `post-test`.
* `CHECKOUT_ID` is the KCIDB checkout ID.

### Production setup

If `CKI_DEPLOYMENT_ENVIRONMENT` is set to `production`, [sentry] support is
enabled via `SENTRY_DSN`. You can the value in your sentry project's settings
under `SDK Setup / Client Keys`.

[Fedora CI messages]: https://pagure.io/fedora-ci/messages/
[CKI documentation]: https://cki-project.org/docs/test-maintainers/testing-builds
[sentry]: https://sentry.io/welcome/
[on Pagure]: https://pagure.io/fedora-ci/messages
