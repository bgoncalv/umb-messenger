"""
Tests for umb_messenger.message_data. Mostly sanity checks to verify content
is set in the correct function.
"""
import copy
import unittest
from unittest import mock

from tests import fakes
from umb_messenger import message_data
from umb_messenger import settings
from umb_messenger import templates

ISSUE = fakes.get_fake_issue_occurrence()
ISSUE_REGRESSION = fakes.get_fake_issue_occurrence(is_regression=True)
TEST_PASS = fakes.get_fake_test(actual_attrs={'status': 'PASS', 'waived': False})
TEST_FAIL = fakes.get_fake_test(actual_attrs={'status': 'FAIL', 'waived': False})
TEST_ERROR = fakes.get_fake_test(actual_attrs={'status': 'ERROR', 'waived': False})
TEST_REGRESSION = fakes.get_fake_test(actual_attrs={'status': 'FAIL', 'waived': False},
                                      issues=[ISSUE_REGRESSION])
TEST_FAIL_ISSUE = fakes.get_fake_test(actual_attrs={'status': 'FAIL', 'waived': False},
                                      issues=[ISSUE])
TEST_ERROR_ISSUE = fakes.get_fake_test(actual_attrs={'status': 'ERROR', 'waived': False},
                                       issues=[ISSUE])
TEST_WAIVED = fakes.get_fake_test(actual_attrs={'status': 'FAIL', 'waived': True})


class TestUnknownIssues(unittest.TestCase):
    """Tests for message_data.get_unknown_issues()."""

    def test_all_pass(self):
        """Verify correctness when all tests passed."""
        self.assertEqual([], message_data.get_unknown_issues([TEST_PASS, TEST_PASS]))

    def test_regression(self):
        """Verify correctness when a test detect a regression."""
        self.assertEqual(
            [TEST_REGRESSION],
            message_data.get_unknown_issues([TEST_ERROR_ISSUE, TEST_FAIL_ISSUE, TEST_REGRESSION])
        )
        self.assertCountEqual(
            [TEST_REGRESSION, TEST_FAIL, TEST_ERROR],
            message_data.get_unknown_issues(
                [TEST_ERROR, TEST_FAIL_ISSUE, TEST_REGRESSION, TEST_FAIL]
            )
        )

    def test_fail(self):
        """Verify correctness when a test failed."""
        self.assertEqual(
            [TEST_FAIL],
            message_data.get_unknown_issues([TEST_ERROR, TEST_FAIL_ISSUE, TEST_FAIL], 'FAIL')
        )

    def test_error(self):
        """Verify correctness when a test errors."""
        self.assertEqual([TEST_ERROR],
                         message_data.get_unknown_issues([TEST_ERROR, TEST_FAIL_ISSUE], 'ERROR'))

    def test_known_issue_pass(self):
        """Verify correctness when known issues are assigned."""
        self.assertEqual([], message_data.get_unknown_issues([TEST_PASS, TEST_FAIL_ISSUE]))


class TestGatingData(unittest.TestCase):
    """Tests for message_data.get_gating_data()."""

    def setUp(self):
        """Set up common data to use."""
        self.builds = [fakes.get_fake_build(actual_attrs={'architecture': 'x86_64',
                                                          'id': 'redhat:11'},
                                            misc={'iid': 12347,
                                                  'package_name': 'kernel',
                                                  'kpet_tree_family': 'rhel'}),
                       fakes.get_fake_build(actual_attrs={'architecture': 's390x',
                                                          'id': 'redhat:10'},
                                            misc={'iid': 12346,
                                                  'package_name': 'kernel-rt',
                                                  'kpet_tree_family': 'rhel',
                                                  'debug': True},
                                            tests=[TEST_REGRESSION]),
                       fakes.get_fake_build(actual_attrs={'architecture': 'zzz',
                                                          'id': 'redhat:15'},
                                            misc={'iid': 12346,
                                                  'package_name': 'kernel-rt',
                                                  'kpet_tree_family': 'rhel-rt',
                                                  'debug': False},
                                            tests=[TEST_PASS]),
                       fakes.get_fake_build(actual_attrs={'architecture': 'x86_64',
                                                          'id': 'redhat:12'},
                                            misc={'iid': 12348,
                                                  'package_name': 'kernel',
                                                  'debug': True,
                                                  'kpet_tree_family': 'rhel-rt'},
                                            tests=[TEST_ERROR])]

        self.checkout = fakes.get_fake_checkout(
            builds=self.builds,
            misc={'brew_task_id': 123456,
                  'scratch': False,
                  'kernel_version': '123.notatest',
                  'source_package_name': 'source-package-name',
                  'iid': 1112},
            attributes={'contacts': ['joe@example.email']},
            actual_attrs={'id': 'redhat:checkout'}
        )

    def _compare_common(self, messages):
        """Verify correctness of common OSCI data."""
        # Data set in a common way, no need to check every message so we're picking
        # some random ones
        self.assertEqual(len(messages), 3)
        self.assertIn('1112', messages[0]['run']['url'])
        self.assertIn('1112', messages[0]['run']['log'])
        self.assertEqual(messages[0]['artifact']['issuer'], 'joe@example.email')
        self.assertEqual(messages[0]['artifact']['id'], 123456)
        self.assertFalse(messages[0]['artifact']['scratch'])
        self.assertEqual(messages[0]['artifact']['component'], 'source-package-name')
        self.assertEqual(messages[1]['artifact']['component'], 'source-package-name')
        self.assertEqual(messages[1]['pipeline']['id'], 'redhat:checkout-x86_64')
        self.assertEqual(messages[1]['artifact']['nvr'], 'source-package-name-123.notatest')

        # Per-arch data
        self.assertEqual(messages[0]['system'][0]['architecture'], 's390x')
        self.assertEqual(messages[1]['system'][0]['architecture'], 'x86_64')
        self.assertEqual(messages[2]['system'][0]['architecture'], 'zzz')
        self.assertEqual(messages[0]['test']['type'], 'tier1-s390x')
        self.assertEqual(messages[1]['test']['type'], 'tier1-x86_64')
        self.assertEqual(messages[2]['test']['type'], 'tier1-zzz')

    def test_fields_finished(self):
        """Verify all expected fields are set for valid data."""
        message = copy.deepcopy(templates.OSCI_FINISHED)
        messages = message_data.get_gating_data(message, self.checkout)

        self._compare_common(messages)

        # Per-arch test data
        self.assertEqual(messages[0]['test']['result'], 'failed')
        # not gating on errors: https://gitlab.com/cki-project/umb-messenger/-/issues/31
        self.assertEqual(messages[1]['test']['result'], 'not_applicable')
        self.assertIn('test aborted, ignoring the result...', messages[1]['test']['note'])
        self.assertEqual(messages[2]['test']['result'], 'passed')

    def test_fields_running(self):
        """Verify all expected fields are set for valid data."""
        message = copy.deepcopy(templates.OSCI_RUNNING)
        messages = message_data.get_gating_data(message, self.checkout)

        self._compare_common(messages)
        self.assertNotIn('result', messages[0]['test'])
        self.assertNotIn('result', messages[1]['test'])
        self.assertNotIn('result', messages[2]['test'])


class TestReadyForTestData(unittest.TestCase):
    """Tests for message_data.get_ready_for_test_data()."""

    def test_pre_test(self):
        """Verify expected values for pre-test notifications are set."""
        message = copy.deepcopy(templates.PRE_TEST)

        builds = [
            fakes.get_fake_build(
                actual_attrs={'architecture': 's390x',
                              'id': 'redhat:10',
                              'output_files': [{'name': 'kernel_package_url', 'url': 'link'}]},
                misc={'iid': 12346,
                      'package_name': 'kernel',
                      'kpet_tree_family': 'fedora',
                      'kpet_tree_name': 'fedora',
                      'debug': True}
            ),
            fakes.get_fake_build(
                actual_attrs={'architecture': 'x86_64',
                              'id': 'redhat:12',
                              'output_files': [{'name': 'kernel_package_url', 'url': 'link2'}]},
                misc={'iid': 12348,
                      'package_name': 'kernel',
                      'kpet_tree_family': 'fedora',
                      'kpet_tree_name': 'fedora'}
            ),
            fakes.get_fake_build(
                actual_attrs={'architecture': 'ppc64le',
                              'id': 'redhat:13',
                              'output_files': [{'name': 'kernel_package_url', 'url': 'link3'}]},
                misc={'iid': 12347,
                      'package_name': 'kernel',
                      'kpet_tree_family': 'fedora',
                      'kpet_tree_name': 'fedora',
                      'testing_skipped_reason': 'unsupported'}
            )
        ]

        checkout = fakes.get_fake_checkout(
            builds=builds,
            misc={'kernel_version': '123.test',
                  'source_package_name': 'kernel-source',
                  'iid': 1112,
                  'patchset_modified_files': [
                      {'path': 'list'}, {'path': 'of'}, {'path': 'files'}
                  ]},
            actual_attrs={'git_repository_branch': 'main', 'id': 'redhat:1'}
        )

        message = message_data.get_ready_for_test_data(message, 'pre_test', checkout)

        self.assertEqual(message['artifact']['issuer'], 'CKI')
        self.assertEqual(message['artifact']['component'], 'kernel-source')
        self.assertEqual(message['artifact']['variant'], 'kernel')
        self.assertEqual(message['branch'], 'main')
        self.assertIn('1112', message['run']['url'])
        self.assertFalse(message['cki_finished'])
        self.assertNotIn('status', message)
        self.assertEqual(message['merge_request']['merge_request_url'], '')

        self.assertEqual(message['checkout_id'], 'redhat:1')
        self.assertEqual(len(message['build_info']), 2)  # One of the builds is unsupported
        self.assertEqual(message['build_info'][0]['architecture'], 's390x')
        self.assertEqual(message['build_info'][0]['build_id'], 'redhat:10')
        self.assertTrue(message['build_info'][0]['debug_kernel'])
        self.assertEqual(message['build_info'][0]['kernel_package_url'], 'link')
        self.assertEqual(message['build_info'][1]['architecture'], 'x86_64')
        self.assertEqual(message['build_info'][1]['build_id'], 'redhat:12')
        self.assertFalse(message['build_info'][1]['debug_kernel'])
        self.assertEqual(message['build_info'][1]['kernel_package_url'], 'link2')

    def test_post_test(self):
        """Verify expected values for post-test notifications are set.

        We don't need to test most of the values as we already do so in pre_test
        and they are set the same way.
        """
        message = copy.deepcopy(templates.POST_TEST)

        builds = [
            fakes.get_fake_build(
                actual_attrs={'output_files': [{'name': 'kernel_package_url', 'url': 'link'}]},
                misc={'package_name': 'kernel-rt'}
            )
        ]
        checkout = fakes.get_fake_checkout(
            builds=builds,
            misc={'kernel_version': '123.test',
                  'iid': 1112,
                  'source_package_name': 'kernel-source'}
        )

        checkout.all.get.return_value.tests = [TEST_ERROR, TEST_PASS, TEST_WAIVED]
        message = message_data.get_ready_for_test_data(message, 'post_test', checkout)
        self.assertTrue(message['cki_finished'])
        self.assertEqual(message['status'], 'error')

        checkout.all.get.return_value.tests = [TEST_PASS, TEST_WAIVED, TEST_FAIL_ISSUE]
        message = message_data.get_ready_for_test_data(message, 'post_test', checkout)
        self.assertEqual(message['status'], 'success')

        checkout.all.get.return_value.tests = [TEST_PASS, TEST_REGRESSION]
        message = message_data.get_ready_for_test_data(message, 'post_test', checkout)
        self.assertEqual(message['status'], 'fail')

    def test_missing_data(self):
        """Verify we don't send messages for builds we can't get info for."""
        message = copy.deepcopy(templates.PRE_TEST)

        builds = [fakes.get_fake_build(), fakes.get_fake_build()]
        checkout = fakes.get_fake_checkout(
            builds=builds,
            misc={'kernel_version': '123.test',
                  'iid': 1112,
                  'source_package_name': 'kernel-source'}
        )

        with self.assertLogs(level='WARNING', logger=settings.LOGGER) as log:
            message = message_data.get_ready_for_test_data(message, 'pre_test', checkout)
            self.assertIn('No data available', log.output[-1])

        self.assertIn('No test data found', message['reason'])

    def test_no_builds(self):
        """Verify we don't send messages for checkouts with no builds."""
        message = copy.deepcopy(templates.PRE_TEST)

        checkout = fakes.get_fake_checkout(
            builds=[],
            misc={'kernel_version': '123.test',
                  'iid': 1112,
                  'source_package_name': 'kernel-source'}
        )

        with self.assertLogs(level='WARNING', logger=settings.LOGGER) as log:
            message = message_data.get_ready_for_test_data(message, 'pre_test', checkout)
            self.assertIn('No data available', log.output[-1])

        self.assertIn('No test data found', message['reason'])

    @mock.patch('umb_messenger.message_data.parse_gitlab_url')
    def test_mr_data(self, mock_parse):
        """Verify MR data for pre-test notifications are correctly set."""
        message = copy.deepcopy(templates.PRE_TEST)

        builds = [
            fakes.get_fake_build(
                actual_attrs={'architecture': 'ppc64le',
                              'id': 'redhat:13',
                              'output_files': [{'name': 'kernel_package_url', 'url': 'link3'}]},
                misc={'iid': 12347,
                      'package_name': 'kernel-rt',
                      'kpet_tree_family': 'fedora',
                      'kpet_tree_name': 'fedora'}
            )
        ]

        checkout = fakes.get_fake_checkout(
            builds=builds,
            misc={'kernel_version': '123.test',
                  'iid': 1112,
                  'source_package_name': 'kernel-source',
                  'patchset_modified_files': [
                      {'path': 'list'}, {'path': 'of'}, {'path': 'files'}
                  ],
                  'related_merge_request': {
                      'url': 'https://link.to.mr',
                      'diff_url': 'https://link.to.diff'
                  }},
            actual_attrs={'git_repository_branch': 'main'}
        )

        fake_mr = fakes.FakeMergeRequest({
            'iid': 123,
            'description': ('Draft: my mr\nBugzilla: https://bz\nrandom line'
                            'with a Bugzilla: do-not-extract\nBugzilla:'
                            'http://extract-this  \nSigned-off-by: myself\n'
                            'JIRA: https://this.is.jira/123'),
            'labels': ['randomlabel', 'Bugzilla::NeedsReview',
                       'Acks::NeedsReview', 'Subsystem:scsi',
                       'Subsystem:qla2xxx'],
            'work_in_progress': True
        })

        mock_parse.return_value = (None, fake_mr)

        message = message_data.get_ready_for_test_data(message, 'pre_test', checkout)

        self.assertEqual(message['merge_request']['merge_request_url'], 'https://link.to.mr')
        self.assertEqual(message['patch_urls'], ['https://link.to.diff'])
        self.assertEqual(message['merge_request']['is_draft'], True)
        self.assertEqual(message['merge_request']['subsystems'], ['scsi', 'qla2xxx'])
        self.assertEqual(message['merge_request']['bugzilla'],
                         ['https://bz', 'http://extract-this'])
        self.assertEqual(message['merge_request']['jira'],
                         ['https://this.is.jira/123'])
        self.assertEqual(message['artifact']['component'], 'kernel-source')
        self.assertEqual(message['artifact']['variant'], 'kernel-rt')
